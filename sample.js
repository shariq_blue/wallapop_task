﻿import React from 'react';
import { StyleSheet, Text, View,ScrollView,Dimensions,AppRegistry,Image,TextInput } from 'react-native';
import AutoResponsive from 'autoresponsive-react-native';
//import ActionButton from 'react-native-action-button';
import { FAB,Searchbar } from 'react-native-paper';
import { Font } from 'expo';

const SCREEN_WIDTH = Dimensions.get('window').width;



export default class Wallapopgrid extends React.Component {

  constructor(props){
    super(props);
    this.state = {
     array: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
     fontLoaded: false
   }
  }

  async componentDidMount() {
    await Font.loadAsync({
      'urba': require('./assets/fonts/Urba.ttf'),
    });

    this.setState({ fontLoaded: true });
  }

  getChildrenStyle() {
    return {
      width: (SCREEN_WIDTH - 18)/2 ,
      height: parseInt(Math.random() * 20 + 12) * 10,
      backgroundColor: '#FFFFFD',
      paddingTop: 20,
      borderRadius: 8,
 borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
    };
  }

  feb(){
    return{
      position: 'absolute',
      margin: 16,
      width:140,
      right: (SCREEN_WIDTH - 158)/2,
      bottom: 0
    }
  }

  getAutoResponsiveProps() {
    return {
      itemMargin: 8,
    };
  }

  renderChildren() {
    const image1=[require('./images/1.jpg'),require('./images/2.jpg'),require('./images/3.jpg'),require('./images/4.jpg'),require('./images/5.jpg')
    ,require('./images/6.jpg'),require('./images/7.jpg'),require('./images/8.jpg'),require('./images/9.jpg'),require('./images/10.jpg')]

    const titles=['Beats headphone','Digital Camera','Leather Strap Watch','Phara Top','Cyan Jacket','Graphics Card','Fit Bit','Gaming Chair',
     'Leather Shoes','Camcorder']

    const prices=['1400€','2200€','400€','800€','200€','1400€','2200€','400€','800€','200€'] 

    return this.state.array.map((i, key) => {
      return (
        <View style={this.getChildrenStyle()} key={key}>
        <Image source={image1[i]} style={styles.image}/>
          
          {
            this.state.fontLoaded ? (
              <View>
                <Text style={styles.pricetag}>{prices[i]}</Text>
                <Text style={styles.productname}>{titles[i]}</Text>
              </View>
             ) : null
           }
        </View>
      );
    }, this);
  
  
  }

  render() {
    const buttonStyle = { width: this.props.size, height: this.props.size, borderRadius: this.props.size / 2, alignItems: "center", justifyContent: "center", borderColor: this.props.borderColor, borderWidth: this.props.borderWidth };

    return (
    <View>
      <ScrollView style={styles.container}>
       <View style={styles.title}>
      <Image source={require('./images/men.png')} style={styles.menu}></Image>         
       {/*
            this.state.fontLoaded ? (
              <Text style={styles.logo}>Wallapop</Text> 
             ) : null
            */ }
        {/*<TextInput placeholder="Search in wallapop" 
         underlineColorAndroid='transparent' 
         style={styles.TextInputStyleClass}
          placeholderTextColor="#000" ></TextInput> */}
            <Searchbar
        placeholder="Search in wallapop"
        placeholderTextColor="black" 
        inputStlye="text"
        //value={"Serarch"}
        style={styles.searchbar}
      />
        </View>

  <Image source={require('./images/filter.jpg')} style={styles.search}></Image> 
        <View style={{flexDirection:"row"}}>
         <Image source={require('./images/car.jpg')}  style={{width:50,height:50,marginLeft:40,marginRight:40}}></Image> 
         <Image source={require('./images/motorrbike.jpg')} style={{width:50,height:50,marginLeft:40,marginRight:40}}></Image> 
         <Image source={require('./images/realstate.jpg')} style={{width:50,height:50,marginLeft:40,marginRight:40}}></Image> 
         </View>
        {
            this.state.fontLoaded ? (
              <Text style={styles.tophead}>Items near you</Text> 
             ) : null
        }
        <AutoResponsive {...this.getAutoResponsiveProps()}>
          {this.renderChildren()}
        </AutoResponsive>
      </ScrollView>
        <FAB
    style={this.feb()}
    small
    icon="add"
    label="List Items"
    onPress={() => console.log('Pressed')}
  />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    //backgroundColor: '#F1EFF0',
    backgroundColor: 'white',
  },
  title: {
    paddingTop: 40,
    paddingBottom: 20,
   // backgroundColor:'#14C2AC',
   backgroundColor:'white',
    flexDirection:'row',
    flex:1
  },
  titleText: {
    color: '#d0bbab',
    textAlign: 'center',
    fontSize: 16,
    fontWeight: 'bold',
  },
  pricetag: {
    textAlign: 'center',
    marginTop:10,
    fontSize: 18,
    fontFamily:'urba',
    color: 'black',
  },
  productname:{
    textAlign: 'center',
    fontSize: 13,
    fontFamily:'urba',
    color: 'silver',
  },
  image: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'contain'
  },
  logo:{
    textAlign:'left',
    fontSize: 26,
    fontFamily:'urba',
    color: 'white',
  },
  menu:{
    marginLeft:10,
    marginRight:20
  },
 search:{
    position: "absolute",top:30, right: 10
  }
,
  tophead:{
    textAlign:'left',
    fontSize: 18,
    fontFamily:'urba',
    color: 'black',
    margin:10
  },
  TextInputStyleClass:{
    //height: 50,
     width:140,
     color:'black',
     borderWidth: 2,
     borderColor: '#F3F3F3',
     borderRadius: 20 ,
     backgroundColor : "#F3F3F3",
     paddingLeft:10
    },
    searchbar:{width:260,backgroundColor:"#F3F3F3",borderRadius:20,height:26,margin:0}
});


AppRegistry.registerComponent('grid',()=>Wallapopgrid)
